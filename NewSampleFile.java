package com.alation.lambda.s3;

public class Constants {
    public static final String COLUMN_TYPE = "column_type";
    public static final String TABLE_TYPE = "table_type";
    public static final String SCHEMA = "SCHEMA";
    public static final String TABLE = "TABLE";
    public static final String DESCRIPTION = "description";
    public static final String TITLE = "title";
    public static final String POSITION = "position";
    public static final String STRING = "string";
    public static final String DATE = "date";
    public static final String NUMERIC = "numeric";
    public static final String PAYLOADS = "payloads";
}
